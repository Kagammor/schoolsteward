// Map
var map = new OpenLayers.Map("map");

var mapbox = new OpenLayers.Layer.XYZ(
    "MapBox Streets",
    [
        "http://a.tiles.mapbox.com/v3/sevenforall.map-mkyzvgrh/${z}/${x}/${y}.png",
		"http://a.tiles.mapbox.com/v3/sevenforall.map-mkyzvgrh/${z}/${x}/${y}.png",
		"http://a.tiles.mapbox.com/v3/sevenforall.map-mkyzvgrh/${z}/${x}/${y}.png"
    ], {
        sphericalMercator: true,
        wrapDateLine: true,
        transitionEffect: "resize",
        buffer: 1,
        numZoomLevels: 18
    }
);
map.addLayer(mapbox);

map.setCenter(
	new OpenLayers.LonLat(5.30,52.20).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	), 7
);

// Markers
var markers = new OpenLayers.Layer.Markers( "Markers" );
map.addLayer(markers);

// Aloysius/De Roosten
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_aloysius = new OpenLayers.Marker(new OpenLayers.LonLat(5.473246,51.428036).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_aloysius);
// Baanderherencollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(5.316572,51.594579).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon));
// BC Broekhin
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_broekhin = new OpenLayers.Marker(new OpenLayers.LonLat(6.001569,51.171834).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_broekhin);
// Bouwens van der Boijecollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_bouwens = new OpenLayers.Marker(new OpenLayers.LonLat(5.978711,51.326245).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_bouwens);
// Comenius Lyceum
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_comenius = new OpenLayers.Marker(new OpenLayers.LonLat(4.840931,52.359822).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_comenius);
// Commanderij College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_commanderij = new OpenLayers.Marker(new OpenLayers.LonLat(5.688509,51.558888).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_commanderij);
// Diamantcollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_diamant = new OpenLayers.Marker(new OpenLayers.LonLat(4.356249,52.094523).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_diamant);
// Dr. Mollercollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_moller = new OpenLayers.Marker(new OpenLayers.LonLat(5.061041,51.686492).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_moller);
// d'Oultremontcollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_doultremont = new OpenLayers.Marker(new OpenLayers.LonLat(5.140369,51.690421).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_doultremont);
// Eckart College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_eckart = new OpenLayers.Marker(new OpenLayers.LonLat(5.500221,51.465274).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_eckart);
// Graaf Huyn College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_graafhuyn = new OpenLayers.Marker(new OpenLayers.LonLat(5.822613,50.957273).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_graafhuyn);
// Hanze College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_hanze = new OpenLayers.Marker(new OpenLayers.LonLat(4.852299,51.636687).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_hanze);
// Het College (Weert)
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_college1 = new OpenLayers.Marker(new OpenLayers.LonLat(5.694012,51.242008).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_college1);
// Het College (Cranendonck)
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_college2 = new OpenLayers.Marker(new OpenLayers.LonLat(5.578018,51.2716).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_college2);
// Het Kwadrant
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_kwadrant = new OpenLayers.Marker(new OpenLayers.LonLat(5.72693,51.250995).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_kwadrant);
// Jacob Roelandslyceum
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_roelands = new OpenLayers.Marker(new OpenLayers.LonLat(5.32925,51.58561).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_roelands);
// Merletcollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_merlet = new OpenLayers.Marker(new OpenLayers.LonLat(5.774756,51.685138).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_merlet);
// Metameer
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_metameer = new OpenLayers.Marker(new OpenLayers.LonLat(5.942665,51.642877).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_metameer);
// Pleincollege Nuenen
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_pleincollege = new OpenLayers.Marker(new OpenLayers.LonLat(5.551654,51.471285).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_pleincollege);
// Praktijkschool Eindhoven
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_praktkeindhoven = new OpenLayers.Marker(new OpenLayers.LonLat(5.478417,51.455957).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_praktkeindhoven);
// Wiringherlant
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_wiringherlant = new OpenLayers.Marker(new OpenLayers.LonLat(5.023402,52.84601).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_wiringherlant);
// Sint-Jan
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_sintjan = new OpenLayers.Marker(new OpenLayers.LonLat(5.928597,50.933582).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_sintjan);
// Sint-Maartenscollege
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_sintmaartens = new OpenLayers.Marker(new OpenLayers.LonLat(5.708918,50.850666).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_sintmaartens);
// Sophianum
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_sophanium = new OpenLayers.Marker(new OpenLayers.LonLat(5.960864,50.807282).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_sophanium);
// Stella Maris College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_stellamaris = new OpenLayers.Marker(new OpenLayers.LonLat(5.757186,50.881354).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_stellamaris);
// Strabrecht College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_stabrecht = new OpenLayers.Marker(new OpenLayers.LonLat(5.57565,51.427681).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_stabrecht);
// Theresialyceum
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_theresia = new OpenLayers.Marker(new OpenLayers.LonLat(5.061552,51.561202).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_theresia);
// 't Wildveld
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_wildveld = new OpenLayers.Marker(new OpenLayers.LonLat(6.163124,51.360926).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_wildveld);
// Willibrordgymnasium
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_willibrord = new OpenLayers.Marker(new OpenLayers.LonLat(5.782533,51.446939).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_willibrord);
// Zwijsen College
var marker_icon = new OpenLayers.Icon('../assets/img/icons/pin.png',new OpenLayers.Size(32,32));
var marker_zwijsen = new OpenLayers.Marker(new OpenLayers.LonLat(5.540262,51.627071).transform(
		new OpenLayers.Projection("EPSG:4326"),
		map.getProjectionObject()
	),marker_icon);
markers.addMarker(marker_zwijsen);
