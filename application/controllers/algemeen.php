<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Algemeen extends CI_Controller {
	public function initiatief()
	{
		$data['content'] = 'algemeen/initiatief'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function nieuws()
	{
		$data['content'] = 'algemeen/nieuws'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function scholen()
	{
		$data['content'] = 'algemeen/scholen'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function fotos()
	{
		$data['content'] = 'algemeen/fotos'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
