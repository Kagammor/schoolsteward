<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leerlingen extends CI_Controller {
	public function index()
	{
		$data['content'] = 'leerlingen/info'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
