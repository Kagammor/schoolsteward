<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function nieuws()
	{
		$data['content'] = 'admin/nieuws'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
