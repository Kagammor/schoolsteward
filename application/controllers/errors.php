<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errors extends CI_Controller {
	public function notfound()
	{
		$data['content'] = 'errors/notfound'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
