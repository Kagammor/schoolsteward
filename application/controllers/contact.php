<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function index()
	{
		$data['content'] = 'contact'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
