<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scholen extends CI_Controller {
	public function info()
	{
		$data['content'] = 'scholen/info'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function po()
	{
		$data['content'] = 'scholen/po'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function vo()
	{
		$data['content'] = 'scholen/vo'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
	
	public function mbo()
	{
		$data['content'] = 'scholen/mbo'; // View to be requested
		$this->load->view('templates/main', $data); // Template to use
	}
}
