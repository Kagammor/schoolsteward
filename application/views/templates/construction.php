<!DOCTYPE html>
<html>
	<head>
		<title>SchoolSteward</title>

		<style type="text/css">
			body {
				font-family: Helvetica, sans-serif;
				text-align: center;
			}
		</style>
	</head>

	<body>
		<img src="<?php echo base_url('/assets/img/logo/header.png'); ?>">
	
		<p>
			De website van SchoolSteward is wegens vernieuwingen op dit moment niet bereikbaar.<br>
			U kunt eventueel de onderstaande contactgegevens gebruiken om contact op te nemen met SchoolSteward/0p ijgen weize.
		</p>

		<p>
			<b>Adres</b><br>
			Beekstraat 32<br>
			5673NA<br>
			Nuenen
		</p>

		<p>
			<b>Telefoon</b><br>
			(+31) 402 907 940<br>
			(+31) 638 040 052
		</p>

		<p>
			<b>E-mail</b><br>
			<a href="mailto:info@oijw.nl">info@oijw.nl</a>
		</p>
	</body>
</html>
