<!DOCTYPE html>
<html>
	<head>
		<title>SchoolSteward</title>
		
		<link href="<?php echo base_url('/assets/css/reset.css'); ?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('/assets/css/global.css'); ?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('/assets/css/specific.css'); ?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url('/assets/css/nav.css'); ?>" type="text/css" rel="stylesheet">

		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script src="http://openlayers.org/api/OpenLayers.js"></script>
	</head>
	
	<body>
		<div id="container">
			<header id="header">
				<a href="<?php echo base_url('/'); ?>"><img src="<?php echo base_url('/assets/img/logo/header.png'); ?>" id="logo"></a>
				
				<nav id="navigation">
					<ul>
						<li>Algemeen
							<ul>
								<a href="<?php echo base_url('algemeen/initiatief'); ?>"><li>Initiatief</li></a>
								<a href="<?php echo base_url('algemeen/nieuws'); ?>"><li>Nieuws</li></a>
								<a href="<?php echo base_url('algemeen/scholen'); ?>"><li>Deelnemende scholen</li></a>
								<a href="<?php echo base_url('algemeen/fotos'); ?>"><li>Foto's</li></a>
							</ul>
						</li>
						
						<a href="<?php echo base_url('leerlingen'); ?>"><li>Voor leerlingen</li></a>
						
						<li>Voor scholen
							<ul>
								<a href="<?php echo base_url('scholen/info'); ?>"><li>Informatie</li></a>
								<a href="<?php echo base_url('scholen/po'); ?>"><li>PO</li></a>
								<a href="<?php echo base_url('scholen/vo'); ?>"><li>VO</li></a>
								<a href="<?php echo base_url('scholen/mbo'); ?>"><li>MBO</li></a>
							</ul>
						</li>
						
						<a href="<?php echo base_url('contact'); ?>"><li>Contact</li></a>
					</ul>
				</nav>
			</header>

			<section id="content">
				<?php
					// Load content
					$this->load->view($content);
				?>
			</section>
		</div>
		
		<footer>
			&copy;<?php echo date('Y'); ?> schoolsteward.nl
			
			<a href="http://www.oijw.nl/" target="_new" title="SchoolSteward is een initiatief van 0p ijgen weize">
				<img src="<?php echo base_url('/assets/img/logo/opeigenweize.png'); ?>">
			</a>
		</footer>
	</body>
</html>
