<h1>Deelnemende scholen</h1>

<p>De volgende scholen nemen op dit moment deel aan ons programma.</p>

<div id="map_container">
	<div id="map"></div>
</div>

<script src="<?php echo base_url('/assets/js/map.js'); ?>"></script>

<ul id="schools">
	<li>
		<h2 id="school_name"><a href="https://pcsintjoris.mwp.nl/AloysiusDeRoosten/Home/tabid/649/Default.aspx" target="_blank">Aloysius/De Roosten</a></h2>
		<span id="school_location">Eindhoven</span>
		
		<a href="https://pcsintjoris.mwp.nl/AloysiusDeRoosten/Home/tabid/649/Default.aspx" target="_blank"><img src="<?php echo base_url('/assets/img/schools/aloysiusderoosten.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.bhc.nl/" target="_blank">Baanderherencollege</a></h2>
		<span id="school_location">Boxtel</span>
		
		<a href="http://www.bhc.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/baanderherencollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.broekhin.nl/" target="_blank">BC Broekhin</a></h2>
		<span id="school_location">Roermond</span>
		
		<a href="http://www.broekhin.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/bcbroekhin.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.hetbouwens.nl/" target="_blank">Bouwens van der Boijecollege</a></h2>
		<span id="school_location">Panningen</span>
		
		<a href="http://www.hetbouwens.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/bouwensvanderboijecollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.comeniuslyceum.nl/Paginas/default.aspx" target="_blank">Comenius Lyceum</a></h2>
		<span id="school_location">Amsterdam</span>
		
		<a href="http://www.comeniuslyceum.nl/Paginas/default.aspx" target="_blank"><img src="<?php echo base_url('/assets/img/schools/comenius.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.commanderijcollege.nl/" target="_blank">Commanderij College</a></h2>
		<span id="school_location">Laarbeek</span><br>
		<span id="school_location">Gemert</span>
		
		<a href="http://www.commanderijcollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/commanderijcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.diamantcollege.nl/" target="_blank">Diamantcollege</a></h2>
		<span id="school_location">Den Haag</span>
		
		<a href="http://www.diamantcollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/diamantcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.drmollercollege.nl/default.aspx?tabid=716" target="_blank">Dr. Mollercollege</a></h2>
		<span id="school_location">Waalwijk</span>
		
		<a href="http://www.drmollercollege.nl/default.aspx?tabid=716" target="_blank"><img src="<?php echo base_url('/assets/img/schools/drmollercollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.doultremontcollege.nl/" target="_blank">d'Oultremontcollege</a></h2>
		<span id="school_location">Drunen</span>
		
		<a href="http://www.doultremontcollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/doultremontcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.eckartcollege.nl/" target="_blank">Eckart College</a></h2>
		<span id="school_location">Eindhoven</span>
		
		<a href="http://www.eckartcollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/eckartcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.ghc.nl/" target="_blank">Graaf Huyn College</a></h2>
		<span id="school_location">Geleen</span>
		
		<a href="http://www.ghc.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/graafhuyncollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.hanze-college.nl/" target="_blank">Hanze College</a></h2>
		<span id="school_location">Oosterhout</span>
		
		<a href="http://www.hanze-college.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/hanzecollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.college.nl/" target="_blank">Het College</a></h2>
		<span id="school_location">Weert</span><br>
		<span id="school_location">Cranendonck</span>
		
		<a href="http://www.college.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/hetcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.hetkwadrant.nl/" target="_blank">Het Kwadrant</a></h2>
		<span id="school_location">Weert</span>
		
		<a href="http://www.hetkwadrant.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/hetkwadrant.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://jrl.nl/" target="_blank">Jacob Roelandslyceum</a></h2>
		<span id="school_location">Boxtel</span>
		
		<a href="http://jrl.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/jacobroelandslyceum.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.merletcollege.nl/" target="_blank">Merletcollege</a></h2>
		<span id="school_location">Mill</span>
		
		<a href="http://www.merletcollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/merletcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.metameer.nl/" target="_blank">Metameer</a></h2>
		<span id="school_location">Boxmeer</span>
		
		<a href="http://www.metameer.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/metameer.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.pleincollegenuenen.nl/" target="_blank">Pleincollege Nuenen</a></h2>
		<span id="school_location">Nuenen</span>
		
		<a href="http://www.pleincollegenuenen.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/pleincollegenuenen.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://pcsintjoris.mwp.nl/PraktijkschoolEindhoven/Home/tabid/206/Default.aspx" target="_blank">Praktijkschool Eindhoven</a></h2>
		<span id="school_location">Eindhoven</span>
		
		<a href="http://pcsintjoris.mwp.nl/PraktijkschoolEindhoven/Home/tabid/206/Default.aspx" target="_blank"><img src="<?php echo base_url('/assets/img/schools/praktijkschooleindhoven.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://wiringherlant.nl/" target="_blank">rsg Wiringherlant</a></h2>
		<span id="school_location">Wieringerwerf</span>
		
		<a href="http://wiringherlant.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/wiringherlant.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://sintjan-lvo.nl/" target="_blank">Sint-Jan</a></h2>
		<span id="school_location">Hoensbroek</span>
		
		<a href="http://sintjan-lvo.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/sintjan.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.sintmaartenscollege.nl/" target="_blank">Sint-Maartenscollege</a></h2>
		<span id="school_location">Maastricht</span>
		
		<a href="http://www.sintmaartenscollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/sintmaartenscollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.sophianum.nl/" target="_blank">Sophianum</a></h2>
		<span id="school_location">Wittem</span>
		
		<a href="http://www.sophianum.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/sophianum.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.stellamariscollege.nl/" target="_blank">Stella Maris College</a></h2>
		<span id="school_location">Valkenburg aan de Geul</span><br>
		<span id="school_location">Meerssen</span>
		
		<a href="http://www.stellamariscollege.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/stellamariscollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://pr.strabrecht.nl/" target="_blank">Strabrecht College</a></h2>
		<span id="school_location">Geldrop</span><br>
		
		<a href="http://pr.strabrecht.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/stabrechtcollege.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.theresialyceum.nl/" target="_blank">Theresialyceum</a></h2>
		<span id="school_location">Tilburg</span>
		
		<a href="http://www.theresialyceum.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/theresialyceum.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.ssonml.nl/wildveld/" target="_blank">'t Wildveld</a></h2>
		<span id="school_location">Venlo</span><br>
		
		<a href="http://www.ssonml.nl/wildveld/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/twildveld.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://www.willibrordgymnasium.nl/" target="_blank">Willibrordgymnasium</a></h2>
		<span id="school_location">Deurne</span>
		
		<a href="http://www.willibrordgymnasium.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/willibrordgymnasium.png'); ?>"></a>
	</li>
	<li>
		<h2 id="school_name"><a href="http://zwijsen.mwp.nl/" target="_blank">Zwijsen College</a></h2>
		<span id="school_location">Veghel</span><br>
		
		<a href="http://zwijsen.mwp.nl/" target="_blank"><img src="<?php echo base_url('/assets/img/schools/zwijsencollege.png'); ?>"></a>
	</li>
</ul>
