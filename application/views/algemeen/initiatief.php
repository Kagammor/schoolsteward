<h1>Initiatief</h1>

<h2>Individualisering van de maatschappij</h2>
<p>Wat ons in de afgelopen jaren steeds meer bezighoudt, is de individualisering van de maatschappij; onze medemens is doorgaans alleen in zichzelf ge&iuml;nteresseerd en heeft weinig kennis van de ongeschreven sociale gedragsregels.</p>
 
<p>Hoe komt het dat we elkaar nauwelijks aanspreken op ons vertoonde gedrag? Waarom geven we zelden complimenten bij goed gedrag, of juist een bewustmakende reactie op een ongewenste handeling?</p>
 
<h2>Simpele vragen, lastige antwoorden</h2>
<p>Op simpele verzoeken als <i>Wil je dat alsjeblieft opruimen?</i> of <i>Wil je alsjeblieft uit de gang vertrekken?</i> worden steeds vaker opstandige antwoorden gegeven als <i>Nee, dat is niet van mij!</i> of <i>Waarom moet ik uit de gang vertrekken?!</i><br>
Voor leerlingen is dit een manier om discussies te starten en zo te ontdekken waar de grenzen van de leerkracht liggen.</p>
 
<p>Stop met discussi&euml;ren en maak samen met 0p ijgen weize leerlingen onderdeel van de oplossing!</p>
