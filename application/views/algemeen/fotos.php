<h1>Foto's</h1>

<p>Beweeg je cursor over een foto om deze groter te bekijken.</p>

<ul id="photos">
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward1.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward1.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward2.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward2.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward3.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward3.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward4.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward4.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward5.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward5.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward6.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward6.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward7.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward7.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward8.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward8.jpg'); ?>">
	</li>
	<li class="thumbnail">
		<img src="<?php echo base_url('/assets/img/photos/thumbnails/steward9.jpg'); ?>">
		<img class="photo" src="<?php echo base_url('/assets/img/photos/steward9.jpg'); ?>">
	</li>
</ul>
