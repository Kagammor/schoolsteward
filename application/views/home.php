<div id="showcase">
	<iframe src="http://player.vimeo.com/video/55982191?title=0&byline=0&portrait=0" width="1000" height="500" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
</div>
			
<div id="front_item_container">
	<div class="front_item">
		<h1>Schoon, rustig, veilig</h1>
		<p>SchoolSteward is een programma voor leerlingen van scholen in heel Nederland, gericht op het verbeteren van de normen en waarden van de leerlingen.</p>
	</div>

	<div class="front_item" id="front_item_right">
		<h1>Sociale controle</h1>
		<p>Het aanleren van sociaal wenselijk gedrag staat bij ons centraal, samengaand met het afleren van onwenselijk gedrag.
		Leerlingen moeten elkaar op een vriendschappelijke manier leren aanspreken, en daarmee bij zichzelf beginnen.</p>
	</div>
</div>

<hr>

<div id="front_news">
	<h1>Nieuws</h1>
	
	<span style="font-style: italic;">Er is momenteel nog geen nieuws.</span>

	<!--	
		<h2 class="news_title">News mock-up header</h2>
		<p><span class="news_date">21-11-2012 | </span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla velit, euismod non feugiat eu, eleifend pharetra sapien. Sed auctor enim vel est sollicitudin viverra. Integer bibendum, nibh eget molestie scelerisque, ipsum nisl dignissim eros, sodales feugiat lectus elit ut augue. Morbi ac orci turpis.</p>
	-->
</div>
