<h1>Primair onderwijs</h1>

<h2>In de praktijk</h2>

<img class="content_photo" src="<?php echo base_url('/assets/img/photos/steward2.jpg'); ?>">

<p>Doen, waarnemen, oefenen en ervaren de leerlingen krijgen handvatten spelenderwijs aangereikt om zelf de verantwoordelijkheid te nemen om de school schoon rustig en veilig te houden. De leerlingen, herkenbaar aan een Stewardjas en portofoon, spreken elkaar aan op gewenst en niet gewenst gedrag. De leerlingen spelen een grote rol om de regels van de school te onderhouden. Het aanspreken van doelgroep tot de doelgroep noemen we peer to peer methode. Het voordeel van deze methode is dat leerlingen dezelfde taal spreken met als resultaat: een schone, rustige en veilige school.</p>
 
<h2>Trainingen</h2>
<p>0p ijgen weize zal op haar eigen wijze de trainingen verzorgen en de klassen voorzien van een theorieles. Voor de Stewards is er ook een praktijkles, zodat de leerlingen zelfverzekerd het plein op kunnen.</p>
 
<p>De theorieles bestaat uit de onderwerpen:
<ul>
	<li>Hoe schoon is mijn school?</li>
	<li>Hoe kan ik afval verminderen (bijv. boterham in broodtrommel i.p.v. boterhamzakje)?</li>
	<li>Wat doe ik als Steward?</li>
	<li>Hoe zie ik eruit als Steward?</li>
	<li>Hoe waarborg ik de sfeer?</li>
	<li>Wat kan wel en wat niet?</li>
	<li>Wat levert het mij op?</li>
</ul>
</p>
 
De praktijkles is er voor de leerlingen die daadwerkelijk de rol van Steward gaan uitoefenen. We staan onder andere stil bij het maken oogcontact, hoe het lichaam in te zetten en wat zeg je tegen de ander? Tijdens de praktijkles gebruiken we onder andere het rollenspel als werkvorm om verschillende situaties te oefenen en de Steward in spe tot in de puntjes voor te bereiden op zijn of haar rol.
