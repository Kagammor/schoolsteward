<h1>Informatie voor scholen</h1>

<h2>Doelstelling</h2>
<p>De doelen die de Stewards voor ogen hebben worden door de school zelf bepaalt. Wij raden aan deze doelen te bespreken met uw accountmanager. Hij zal u door ervaring kunnen vertellen wat haalbaar is en in welk tijdspad u de doelen kunt realiseren.</p>
 
<h2>Waar</h2>
<p>Elke school bepaalt zelf waar en wanneer de Stewards actief zijn. Veelal wordt gekozen voor de aula, gangen, poort, fietsenstallingen en het plein. Vanzelfsprekend kunt u Stewards ook op andere plekken inzetten. Uiteraard kan 0p ijgen weize u bij deze keuze helpen.</p>
 
<h2>Subsidie</h2>
<p>Er zijn diverse subsidieregelingen. U kunt gebruik maken van de subsidie Maatschappelijke Stage, de subsidie Veilige Scholen en de subsidie voor Leren, Loopbaan en Burgerschap.</p>
 
<h2>Verdienmodel</h2>
<p>In het eerste jaar dat Schoolsteward wordt ingezet is de investering het grootst, omdat alle leerjaren training krijgen. In de vervolgjaren komen alleen de nieuwe leerlingen in aanmerking voor een theorieles en de nieuwe Stewards voor een praktijkles.
Door de afname van surveillanten en de afname van schoonmaakkosten bespaart u op de kosten en verdient u op termijn de investeringen terug.</p>
