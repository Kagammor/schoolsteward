<h1>Voortgezet onderwijs</h1>

<h2>In de praktijk</h2>

<img class="content_photo" src="<?php echo base_url('/assets/img/photos/steward4.jpg'); ?>">

<p>Leerlingen, herkenbaar aan een Stewardjas en portofoon, spreken elkaar aan op niet gewenst gedrag. De leerlingen spelen hiermee dus een grote rol om de regels van de school te onderhouden. Het aanspreken van doelgroep tot de doelgroep noemen we peer to peer-methode. Het voordeel van deze methode is dat leerlingen dezelfde taal spreken met als resultaat: een schone, rustige en veilige school.</p>
 
<h2>Trainingen</h2>
<p>0p ijgen weize zal op haar eigen wijze de trainingen verzorgen en alle klassen voorzien van een theorieles. Het leerjaar dat de Steward gaat zijn, krijgt eveneens een uitgebreide praktijkles.</p>

<p>De theorieles is er voor elke leerling op school en bestaat uit een training van 1 lesuur. Na de training weet de leerling:

<ul>
	<li>Wat de reden is van invoering (normen & waarden)</li>

<img class="content_photo" src="<?php echo base_url('/assets/img/photos/steward6.jpg'); ?>">

	<li>Wie worden er Steward</li>
	<li>Wat doet een Steward</li>
	<li>Wat levert dit jou op</li>
	<li>Wat wordt er van mij verwacht (respect)</li>
	<li>Wat kan ik van de Steward verwachten.</li>
</ul>
</p>
 
<p>De praktijkles is er voor de leerlingen die daadwerkelijk de rol van Steward gaan uitoefenen. Tijdens de praktijkles gebruiken we het rollenspel als hulpmiddel om verschillende situaties te oefenen en de Steward in spe tot in de puntjes voor te bereiden op zijn of haar taak.</p>
