<h1>MBO</h1>

<h2>Steward als sociaal-maatschappelijke dimensie</h2>
<p>Naast vakinhoudelijke kennis en vaardigheden is het maatschappelijk bewustzijn, een goede beroepshouding en professioneel gedrag van groot belang. Het programma SchoolSteward speelt een steeds belangrijkere rol in het verwerven van deze zogenaamde burgerschapscompetenties. Het cre&euml;ren van dit bewustzijn en het ontwikkelen van verantwoord gedrag en houding is een kwestie van ervaren en doen.</p>
 
<h2>Sociaal-maatschappelijke dimensie</h2>
<p>De sociaal-maatschappelijke dimensie heeft betrekking op de bereidheid en het
vermogen om deel uit te maken van de gemeenschap en daar een actieve bijdrage aan te leveren.</p>

<p>Het gaat hier om het adequaat functioneren in de eigen woon- en leefomgeving, in
zorgsituaties en in de school; om de acceptatie van verschillen en culturele verscheidenheid. Om adequaat te kunnen functioneren in de sociale omgeving is het nodig dat de deelnemer de aspecten van breed geaccepteerde sociale omgangsvormen kent en deze kan toepassen in verschillende situaties. De deelnemer heeft inzicht in de kenmerken van verschillende culturen. In zijn opvattingen en gedrag toont hij respect voor culturele verscheidenheid. De deelnemer heeft kennis over en inzicht in de volgende onderwerpen die bij de sociaalmaatschappelijke dimensie aan bod komen: de grondrechten en plichten in Nederland, kenmerken van de verschillende (sub)culturen in Nederland, kenmerken van - en oorzaken van spanningen tussen - verschillende (sub)culturen en bevolkingsgroepen in Nederland, kenmerken van ethisch en integer handelen, en het doel en de invloed van sociale en professionele netwerken.</p>
 
<h2>Competenties</h2>
<p>Neemt deel in diverse sociale verbanden en leeft in de openbare ruimte
<ul>
	<li>Beslissen en activiteiten initi&euml;ren</li>
	<li>Aandacht en begrip tonen</li>
	<li>Samenwerken en overleggen</li>
	<li>Ethisch en integer handelen</li>
	<li>Onderzoeken</li>
</ul>
</p>
 
<p>Voert activiteiten uit voor de leefbaarheid van zijn sociale omgeving
<ul>
	<li>Beslissen en activiteiten initi&euml;ren</li>
	<li>Samenwerken en overleggen</li>
	<li>Plannen en Organiseren</li>
	<li>Instructies en procedures opvolgen</li>
</ul>
</p>
