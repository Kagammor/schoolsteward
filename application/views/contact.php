<h1>Contact</h1>

<p>Om contact op te nemen met <a href="http://www.oijw.nl/">0p ijgen weize</a>, de organisatie achter SchoolSteward, kunt u de volgende gegevens gebruiken:</p>
<p>
	<ul id="address">
		<li>Adres</b>
			<ul>
				<li>
					Beekstraat 32<br>
					5673NA<br>
					Nuenen
				</li>
			</ul>
		</li>
	
		<li>Telefoon</b>
			<ul>
				<li id="address_phone">(+31) 402 907 940</li>
				<li id="address_mobile">(+31) 638 040 052</li>
			</ul>
		</li>
	
		<li>E-mail</b>
			<ul>
				<li><a href="mailto:info@oijw.nl">info@oijw.nl</a></li>
			</ul>
		</li>
	</ul>
</p>

<!--
<br>

<h2>Brochure</h2>
<p>
	Voor het aanvragen van een brochure verzoeken wij u om het onderstaande formulier te gebruiken.<br>
	Het invullen van de dikgedrukte velden is noodzakelijk voor een spoedige en juiste verwerking.
</p>
<form id="contact" method="POST">
	<ul>
		<li>
			<label for="naam"><b>Naam:</b></label>
			<input type="text" name="naam">
		</li>

		<li>
			<label for="email"><b>E-mail:</b></label>
			<input type="text" name="email">
		</li>

		<li>
			<label for="locatie"><b>Locatie:</b></label>
			<input type="text" name="locatie">
		</li>

		<li>	
			<label for="telnummer">Telefoonnummer:</label>
			<input type="text" name="telnummer">
		</li>

		<li>	
			<label for="type"><b>Type brochure:</b></label>
			<select name="type">
				<option value="po">Basisonderwijs</option>
				<option value="vo">Voortgezet Onderwijs</option>
				<option value="mbo">Middelbaar Beroepsonderwijs</option>
			</select>
		</li>

		<li>
			<label for="details"><b>Bericht:</b></label><br>
			<textarea name="bericht"></textarea>
		</li>

		<input type="submit" name="submit" value="Aanvragen">
	</ul>
</form>
-->
