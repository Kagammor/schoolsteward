<h1>Informatie voor leerlingen</h1>
 
<h2>Competenties</h2>
<p>Tijdens je taak als Steward werk je aan de volgende competenties:

<img class="content_photo" src="<?php echo base_url('/assets/img/photos/steward9.jpg'); ?>">

<ul>
	<li>Beslissingen nemen en activiteiten initi&euml;ren.</li>
	<li>Aandacht en begrip tonen</li>
	<li>Samenwerken en overleggen</li>
	<li>Ethisch en integer handelen</li>
	<li>Onderzoeken</li>
	<li>Plannen en Organiseren</li>
	<li>Instructies en procedures opvolgen</li>
</ul></p>

<p>Als je Steward bent op het middelbaar onderwijs kun je, in overleg met de school, dit (deels) laten gelden als <i>Maatschappelijke Stage</i> (MaS). Stewards op het MBO voldoen ermee aan de verplichte <i>sociaal maatschappelijke dimensie</i>.</p>

<h2>Certificaat</h2>
<p>Als je je taak als Steward goed hebt afgerond krijg je een certificaat. De school bepaald de beoordeling die op dit certificaat staat. De betrefende certificaten worden gratis verstrekt door 0p ijgen weize, mits de school jaarlijks de trainingen door ons laat verzorgen.</p>
